package ru.shokin.tm.controller;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.service.ProjectService;
import ru.shokin.tm.service.ProjectTaskService;
import ru.shokin.tm.service.UserService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService,
                             UserService userService)
    {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createProject() {
        System.out.println("Create project");
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        final Long userId = userService.currentUser.getId();
        if (projectService.create(userId, name, description) == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot create project with null argument.");
            System.out.println("Please, create project again :)");
            return -1;
        }
        System.out.println("ok");
        return 0;
    }

    public int clearProject() {
        System.out.println("Clear project");
        projectService.clear();
        System.out.println("ok");
        return 0;
    }

    public int listProject() {
        System.out.println("Projects list");
        final Long userId = userService.currentUser.getId();
        viewProjects(projectService.findAllByUserId(userId));
        System.out.println("ok");
        return 0;
    }

    public int viewProjectById() {
        System.out.println("Please, enter project id:");
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Project project = projectService.findByUserIdAndId(userId, id);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() {
        System.out.println("Please, enter project name:");
        final Long userId = userService.currentUser.getId();
        final String name = scanner.nextLine();
        final Project project = projectService.findByUserIdAndName(userId, name);
        viewProject(project);
        return 0;
    }

    public int updateProjectById() {
        System.out.println("Update project by id");
        System.out.println("Please, enter project id:");
        final Long userId = userService.currentUser.getId();
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = projectService.findByUserIdAndId(userId, id);
        if (project == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        projectService.update(id, name, description);
        System.out.println("ok");
        return 0;
    }

    public int updateProjectByName() {
        System.out.println("Update project by name");
        System.out.println("Please, enter project name:");
        final Long userId = userService.currentUser.getId();
        final String projectName = scanner.nextLine();
        final Project project = projectService.findByUserIdAndName(userId, projectName);
        if (project == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter new project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter new project description:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("ok");
        return 0;
    }

    public int removeProjectById() {
        System.out.println("Remove project by id");
        System.out.println("Please, enter project id:");
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(userId, id);
        if (project == null) {
            System.out.println("failed");
        } else {
            projectTaskService.removeProjectWithTasks(userId, id);
            System.out.println("ok");
        }
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("Remove project by name");
        System.out.println("Please, enter project name:");
        final Long userId = userService.currentUser.getId();
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(userId, name);
        if (project == null) {
            System.out.println("failed");
        } else {
            final long projectId = project.getId();
            projectTaskService.removeProjectWithTasks(userId, projectId);
            System.out.println("ok");
        }
        return 0;
    }

    private void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("View project");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("***");
        System.out.println("User ID: " + project.getUserId());
        System.out.println("ok");
    }

    public void viewProjects(List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        Collections.sort(projects, Comparator.comparing(Project::getName));
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
    }

}