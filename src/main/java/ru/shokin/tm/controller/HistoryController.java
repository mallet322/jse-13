package ru.shokin.tm.controller;


import ru.shokin.tm.service.HistoryService;

public class HistoryController {

    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    public void addCommand(final String command) {
        historyService.create(command);
    }

    public int historyList() {
        System.out.println("Last entered 10 commands");
        historyService.viewHistory();
        System.out.println("ok");
        return 0;
    }

}