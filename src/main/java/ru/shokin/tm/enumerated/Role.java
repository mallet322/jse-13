package ru.shokin.tm.enumerated;

public enum Role {

    ADMIN("Administrator"),
    USER("User");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}